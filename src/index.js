//@flow
module.exports.Phonelib = require("./phonelib");
module.exports.Constants = require("./constants");

module.exports.Locations = require("../meta/Locations.json");
module.exports.Relations = require("../meta/Relations.json");

module.exports.Utils = require("./utils");

export type { PhoneNumber } from "./phonelib";

export type {
  User,
  Message,
  GenderType,
  FeedCard,
  Feed,
  QuestionCard,
  TagmixyToken,
  ContactListCard
} from "./types";

// {
//   /*
// export const extractHashTags = message => {
//   if (isEmpty(message)) return [];
//   const regex = /(#[a-z\*\d-]+)/gi;

//   let arr = message.match(regex);
//   arr = arr ? arr.map(t => t.replace("#", "")).map(_.toLower) : [];
//   arr = arr.filter(elem => elem.indexOf("*") < 0);
//   return arr.filter((elem, pos, arr) => arr.indexOf(elem) == pos);
// }; */
// }
