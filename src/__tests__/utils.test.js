const Utils = require('../utils')

describe("Utils test", () => {
  test("Locations read properly", () => {
    expect(Utils.isEmptyOrBlank(" ")).toBe(true)
    expect(Utils.isEmptyOrBlank(undefined)).toBe(true)
    expect(Utils.isEmptyOrBlank(null)).toBe(true)
    expect(Utils.isEmptyOrBlank("as")).toBe(false)
  })
  test("Uid valid", () => {
    expect(Utils.isValidUid(" ")).toBe(false)
    expect(Utils.isValidUid(undefined)).toBe(false)
    expect(Utils.isValidUid(null)).toBe(false)
    expect(Utils.isValidUid("as")).toBe(false)
    expect(Utils.isValidUid("0x123")).toBe(true)
    expect(Utils.isValidUid("0x123AF")).toBe(true)
  })
})