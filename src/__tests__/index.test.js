const {
  Locations,
  Relations,
} = require('../index')

describe("Locations Test", () => {
  test("Locations read properly", () => {
    expect(Array.isArray(Locations)).toBe(true)
    expect(Locations.includes("college")).toBe(true)
  })
})

describe("Relations Test", () => {
  test("Relations read properly", () => {
    expect(Array.isArray(Relations)).toBe(true)
    expect(Relations.includes("friend")).toBe(true)
  })
})