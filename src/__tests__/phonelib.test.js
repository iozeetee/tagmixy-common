const {
  parseNumber,
  parsePhoneNumber,
  isValidNumber,
  isValidCCA2,
  getCCA2OfNumber,
} = require('../index').Phonelib

const { isEmpty } = require('lodash')

describe("PhoneLib Tests", () => {

  test("test local newzealand number", () => {
    expect(() => parsePhoneNumber("02102257520")).toThrowError('INVALID_COUNTRY')
    expect(parsePhoneNumber("02102257520","US").isValid()).toBe(false)
    expect(parsePhoneNumber("02102257520","NZ").isValid()).toBe(true)
  })

  test("valid", () => {
    expect(isValidNumber("+642102257520","US")).toBe(true)
    expect(isValidNumber("+642102257520","NZ")).toBe(true)
    expect(isValidNumber("+642102257520")).toBe(true)
    expect(isValidNumber("+sd")).toBe(false)
    expect(isValidNumber("02102257520")).toBe(false)
    expect(isValidNumber("02102257520","NZ")).toBe(true)
    expect(isValidNumber("asdf","NZ")).toBe(false)
    expect(isValidNumber({},"NZ")).toBe(false)
  })

  test("isValidCCA2", () => {
    expect(isValidCCA2("US")).toBe(true)
    expect(isValidCCA2("NZ")).toBe(true)
    expect(isValidCCA2("")).toBe(false)
    expect(isValidCCA2("  ")).toBe(false)
    expect(isValidCCA2(undefined)).toBe(false)
    expect(isValidCCA2("USA")).toBe(false)
  })

  test("getCCA2OfNumber", () => {
    expect(getCCA2OfNumber("+6421033357520")).toBe("NZ")
    expect(getCCA2OfNumber("021033357520")).toBe(undefined)
    expect(getCCA2OfNumber(null)).toBe(undefined)
    expect(getCCA2OfNumber("asdfasdf")).toBe(undefined)
  })

  
});