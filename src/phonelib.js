// @flow
export interface PhoneNumber {
  country?: string;
  nationalNumber: string;
  number: string;
  isPossible(): boolean;
  isValid(): boolean;
  formatNational(options?: {}): string;
  formatInternational(options?: {}): string;
}

const {
  isValidNumber: isValidNumberCustom,
  formatNumber: formatNumberCustom,
  parsePhoneNumber: parsePhoneNumberCustom
} = require("libphonenumber-js/custom");

const { isEmptyOrBlank } = require("./utils");

const { isEmpty, trim } = require("lodash");

const metadata = require("../meta/metadata.mobile.json");

const formatNumber = (...args: any): string =>
  formatNumberCustom(...args, metadata);

const isValidNumber = (...args: any): boolean => {
  try{
    return isValidNumberCustom(...args, metadata);
  }catch(e){
    return false
  }
}

const parsePhoneNumber = (...args: any): PhoneNumber =>
  parsePhoneNumberCustom(...args, metadata);

const getCCA2OfNumber = (number: string): string | void => {
  try {
    return parsePhoneNumber(number).country;
  } catch (e) {
    return undefined;
  }
};

const isValidCCA2 = (cca2: string):boolean => {
  if (isEmptyOrBlank(cca2)) return false;
  return !!metadata.countries[cca2]
};

module.exports = {
  metadata,
  formatNumber,
  isValidNumber,
  parsePhoneNumber,
  getCCA2OfNumber,
  isValidCCA2
};
