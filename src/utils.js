// @flow
const { isEmpty, trim } = require("lodash");

const isEmptyOrBlank = (term: string | null) => {
  return !term || isEmpty(trim(term))
};

const strToU8 = (str:string) => {
  return new Uint8Array(new Buffer(str));
}

const isValidUid= (uid:string) => {
  const pattern = /^([0-9A-Fa-fx]+|me)$/;
  return !isEmptyOrBlank(uid) && pattern.test(uid);
}

module.exports = {
  isEmptyOrBlank,
  strToU8,
  isValidUid,
};
