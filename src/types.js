//@flow
export type TagmixyToken = {
  cca2: string,
  phone: string,
  name: string,
  sex: GenderType
};

export type GenderType = "MALE" | "FEMALE" | "NEUTRAL";

export type User = {
  type: "user",
  uid: string,
  phone: string,
  name?: string,
  sex?: GenderType,
  location?: string,
  relation?: string,
  messages?: Array<Message>
};

export type Message = {
  type: "message",
  uid: string,
  message: string,
  status: number,
  createdOn: number,
  location?: string,
  relation: string,
  givenTo?: Array<{ phone: string }>,
  givenBy?: Array<{ sex: GenderType }>,
  schema?: void
};

//question : use template $$$contact$$ to insert contact .. $$$#taglabel$$ to put tag
export type QuestionCard = {
  type: "questionCard",
  id: string,
  question: string,
  answer: string,
  skip: boolean,
  phone: string,
  schema?: void
};

export type ContactListCard = {
  type: "contactListCard",
  id: string,
  phones: Array<string>,
  schema?: void
};

export type FeedCard = QuestionCard | ContactListCard | Message;

export type Feed = {
  cards: Array<FeedCard>
};
